#
#  Be sure to run `pod spec lint NewtonSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "NewtonSDK"
  s.version      = "0.0.1"
  s.summary      = "A summar on NewtonSDK. This should be extended at a later point."
  s.description  = "The description of NewtonSDK.  This should be extended at a later point."
  s.license      = "No distribution"
  s.authors      = "deloittedigital"

  # This home page should be updated
  s.homepage     = "https://bitbucket.org/si_ma/newtonsdk/overview"

  s.platform     = :ios, "9.1"
  s.source       = { :git => "https://si_ma@bitbucket.org/si_ma/newtonsdk.git", :tag => s.version }
  s.source_files  = "NewtonSDK", "NewtonSDK/**/*.{h,swift}"

  # s.public_header_files = "Classes/**/*.h"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  s.requires_arc = true
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }
  s.dependency "SwaggerClient", "~> 1.0"

end
